const { UniqueConstraintError, DataTypes } = require('sequelize');
module.exports = class Table {
    get fields() {
        return this._fields;
    }

    get idcolumn() {
        return this._idcolumn;
    }

    constructor(table, ignoredFields) {
        let err = new Error();
        this._initPromise = (async () => {
            try {
                this._table = await table;
                this._fields = Object.keys(this._table.rawAttributes);
                this.tablename = this._table.tableName;
                this._idcolumn = this._table.primaryKeyAttribute;
                this._ignoredFields = ignoredFields;
                if (this._table.primaryKeyAttributes.length > 1) {
                    this._idcolumn = this._table.primaryKeyAttributes;
                } else {
                    this._autoincrement = this._table.rawAttributes[this._idcolumn].autoIncrement;
                }
            } catch (e) {
                err.message = e.message;
                err.stack = e.stack + '\n' + err.stack;
                throw err;
            }
        })();
    }

    /**
     * Get all elements from the table. A query object can optionally be specified to 
     * @param {object} object an object containing the restrictions for the query
     * @returns Array of query results
     */
    async getAll(object) {
        await this._initPromise;
        object = (object) ? object : {};
        const keys = Object.keys(object);
        let query = {};
        for (const field of this.fields) {
            if (keys.indexOf(field) != -1) {
                query[field] = object[field];
            }
        }
        const res = await this._table.findAll({ where: query, attributes: { exclude: this._ignoredFields } });
        return res.map(r => r.dataValues);
    }

    async getById(id) {
        await this._initPromise;
        let isArray = Array.isArray(this._idcolumn);
        if (isArray) {
            if (typeof id != 'object') {
                throw new Error('TableError: Id has to be provided as object!');
            }
            const notPartOfKey = Object.keys(id).filter(i => this.idcolumn.indexOf(i) == -1);
            if (notPartOfKey.length > 0) {
                // Field is not part of the primary key
                const error = new Error(`TableError: ${notPartOfKey} is not part of the primary key of the table ${this.tablename}!`);
                throw error;
            }
            const missingKeys = this._idcolumn.filter(i => Object.keys(id).indexOf(i) == -1);
            if (missingKeys.length > 0) {
                // Key field(s) is/are missing for a search by the ID/Primary key
                const error = new Error(`TableError: ${missingKeys} is/are missing for a search by the primary key of the table ${this.tablename}!`);
                throw error;
            }
            return (await this.getAll(id))[0] || null;
        }
        else {
            let result = await this._table.findByPk(id);
            return result ? result.dataValues : null;
        }
    }

    async create(object) {
        await this._initPromise;
        const keys = Object.keys(object);
        if (Array.isArray(this.idcolumn)) {
            for (let idcol of this.idcolumn) {
                if (keys.indexOf(idcol) == -1) {
                    throw new Error(`${this.tablename}: id[${idcol}] not present`);
                }
            }
        }
        else if (keys.indexOf(this.idcolumn) == -1 && !this._autoincrement) {
            throw new Error(`${this.tablename}: id[${this.idcolumn}] not present`);
        }
        let updatefields = {};
        for (const field of this.fields) {
            updatefields[field] = object[field];
        }
        try {
            return await this._table.create(updatefields);
        } catch (err) {
            if (err instanceof UniqueConstraintError) {
                throw new Error(`0:2: unique constrait error for fields ${err.fields.join(', ')}`, err);
            }
            else {
                throw err;
            }
        }
    }

    async update(object) {
        await this._initPromise;
        const keys = Object.keys(object);
        if (Array.isArray(this.idcolumn)) {
            for (let idcol of this.idcolumn) {
                if (keys.indexOf(idcol) == -1) {
                    throw new Error(`${this.tablename}: id[${idcol}] not present`);
                }
            }
        }
        else if (keys.indexOf(this.idcolumn) == -1) {
            throw new Error(`${this.tablename}: id[${this.idcolumn}] not present`);
        }
        let updatefields = {};
        let key = {};
        for (const field of this.fields) {
            if (keys.indexOf(field) != -1) {
                if (Array.isArray(this.idcolumn)) {
                    if (this.idcolumn.indexOf(field) > -1) {
                        key[field] = object[field];
                    } else {
                        updatefields[field] = object[field];
                    }
                } else {
                    if (this.idcolumn == field) {
                        key[field] = object[field];
                    } else {
                        updatefields[field] = object[field];
                    }
                }
            }
        }

        const result = await this._table.update(updatefields, { where: key });
        if (result[0] != 1) {
            throw new Error('0:1:row does not exist in database');
        }
        return result;
    }

    async delete(object) {
        await this._initPromise;
        const keys = Object.keys(object);
        if (Array.isArray(this.idcolumn)) {
            for (let idcol of this.idcolumn) {
                if (keys.indexOf(idcol) == -1) {
                    throw new Error(`${this.tablename}: id[${idcol}] not present`);
                }
            }
        }
        else if (keys.indexOf(this.idcolumn) == -1) {
            throw new Error(`${this.tablename}: id[${this.idcolumn}] not present`);
        }
        let key = {};
        for (const field of this.fields) {
            if (keys.indexOf(field) != -1) {
                if (Array.isArray(this.idcolumn)) {
                    if (this.idcolumn.indexOf(field) > -1) {
                        key[field] = object[field];
                    }
                }
                else {
                    if (this.idcolumn == field) {
                        key[field] = object[field];
                    }
                }
            }
        }
        const result = await this._table.destroy({ where: key });
        if (result != 1) {
            throw new Error('0:1:row does not exist in database');
        }
        return result;
    }
};