var { query } = require('../lib');

module.exports = class Join {

    constructor(table1, table2, joinColumn1, joinColumn2) {
        this.table1 = table1;
        this.table2 = table2;
        this.joinColumn1 = joinColumn1;
        this.joinColumn2 = joinColumn2;
        this._baseSelectQuery = `select * from ${this.table1.tablename}
        join ${this.table2.tablename} 
        on ${this.table1.tablename}.${this.joinColumn1} = ${this.table2.tablename}.${this.joinColumn2 || this.joinColumn1} 
        where 1=1 `.replace(/\n/g, '');

    }

    _getQuerWhere(table, keys, select, params, object) {
        for (const field of table.fields) {
            if (keys.indexOf(field) != -1) {
                if (Array.isArray(object[field]) && object[field].length > 0) {
                    select += 'and ( 0=1';
                    for (let item of object[field]) {
                        select += ` or ${table.tablename}.${field} =? `;
                        params.push(item);
                    }
                    select += ') ';
                } else {
                    select += ` and ${table.tablename}.${field} =? `;
                    params.push(object[field]);
                }
            }
        }
        return select;
    }

    async getAll(object) {
        await this.table1._initPromise;
        await this.table2._initPromise;

        const keys = Object.keys(object);
        let select = this._baseSelectQuery;
        let params = [];

        select = this._getQuerWhere(this.table1, keys, select, params, object);
        select = this._getQuerWhere(this.table2, keys, select, params, object);

        try {
            return await query(select, params);
        } catch (e) {
            e.message += ` for join ${this.table1.tablename} ${this.table2.tablename}`;
        }
    }
};