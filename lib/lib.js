const errorregex = /\d+:\d+.*/;

/**
 * Hanles Errors in express request handlers
 * @param {*Exception} err The Error thrown by the promisechain/function called
 * @param {*Express.response} res The Response Object of Express
 */
function handleerror(err, res) {
    //if for some reason, a string is thrown, we now for sure end up with a message string.
    let message = err.message ? err.message : err;
    if (errorregex.test(message)) {
        res.status(400).send(message);
    } else {
        console.error(err);
        console.error(err.stack);
        res.status(400).send('bad request');
    }
}
module.exports.handleerror = handleerror;

function json(res) {
    return (data) => res.json(data);
}
module.exports.json = json;

function ok(res) {
    return () => res.send('ok');
}
module.exports.ok = ok;

function error(res) {
    return (err) => handleerror(err, res);
}
module.exports.error = error;