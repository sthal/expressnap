const express = require('express');
var permissionControl = require('../permissions');

/**
 * Class to use in routes where certain permissions are required for all subs.
 * Do not use in public routes.
 * 
 * Can be specified for all request methods in batch or for each
 * request method specificially.
 * 
 * use e.g.
 * let permissionManager = require('../db/permissionmanager');
 * let routePermission = new permissionManager();
 * routePermission.addRoutePermission(<permissionname>);
 * router.use(routePermission.build());
 */
module.exports = class PermissionManager {
    constructor() {
        this._getPermissions = [];
        this._postPermissions = [];
        this._putPermissions = [];
        this._deletePermissions = [];
    }

    /**
     * Sets the permissions for all request methods.
     * 
     * @param {*} permissions string or array containing required Permissions
     */
    addRoutePermission(permissions) {
        this.addGet(permissions);
        this.addPost(permissions);
        this.addPut(permissions);
        this.addDelete(permissions);
        return this;
    }

    addGet(permission) {
        this._getPermissions.push(permission);
        return this;
    }
    addPost(permission) {
        this._postPermissions.push(permission);
        return this;
    }
    addPut(permission) {
        this._putPermissions.push(permission);
        return this;
    }
    addDelete(permission) {
        this._deletePermissions.push(permission);
        return this;
    }

    setHasPermissionMiddleware(middleware) {
        this._hasPermissionFunction = middleware;
        return this;
    }

    build() {
        const router = express.Router();
        router.use((req, res, next) => {
            if (req.method == 'GET') {
                this._checkPermissions(this._getPermissions, req, res, next);
            } else if (req.method == 'POST') {
                this._checkPermissions(this._postPermissions, req, res, next);
            } else if (req.method == 'PUT') {
                this._checkPermissions(this._putPermissions, req, res, next);
            } else if (req.method == 'DELETE') {
                this._checkPermissions(this._deletePermissions, req, res, next);
            } else {
                res.status(500).send('0:1:internal server error');
            }
        });
        return router;
    }

    /**
     * Checks if a user has the required permission(s) for the accessed route.
     * 
     * @param {*} permissions string or array of permissions
     * @param {*} req request object
     * @param {*} res response object
     * @param {*} next next function
     */
    async _checkPermissions(permissions, req, res, next) {
        if (permissions.length == 0) {
            console.error('Permission for ' + req.url + ' (' + req.method + ') is not set.');
            res.status(400).send('0:1:internal server error');
            return;
        } else if (permissions.length > 0) {
            let hasPermission = false;
            for (let p of permissions) {
                hasPermission = await permissionControl['get' + p](req);
                if (hasPermission === true) {
                    continue;
                } else {
                    res.status(401).send('1:2:unauthorized');
                    return;
                }
            }
            if (this._hasPermissionFunction) {
                req = this._hasPermissionFunction(req);
            }
            next();
            return;
        } else {
            res.status(400).send('0:1:internal server error');
            return;
        }
    }
};