const express = require('express');
const Table = require('../table');
const { json, error, handleerror } = require('../lib');

module.exports = class ApiBuilder {
    constructor(routename, table, ignoredFields) {
        this._routename = routename;
        this._routeTable = new Table(table, ignoredFields);
        this._hasDelete = true;
        this._hasPut = true;
        this._hasGet = true;
        this._hasPost = true;
    }

    /**
     * Adds a middleware function for the api route. Calling this function multiple times will overwrite the previous middleware function.
     * @param {function(req,res,next)} middleare Express middleware function that is hooked up in the same scope as the api call and executed before the api call
     * @returns ApiBuilder instance
     */
    setBeforeMiddleware(middleare) {
        this._beforefunction = middleare;
        return this;
    }

    /**
     * Add a callback function that is called after an entry has been created. Calling this function multiple times will overwrite the previous callback function.
     * @param {function(req,data:object)} afterCreateMiddleware callback function containing the express request obect and the created element
     * @returns ApiBuilder instance
     */
    setAfterCreateMiddleware(afterCreateMiddleware) {
        this._afterCreateMiddleware = afterCreateMiddleware;
        return this;
    }

    /**
     * Add a callback function that is able to modify the results of a get request. The result of this function will be returned as json.
     * middleware function has to return a single promise.
     * 
     * @param {function(req, data:[object])} middleware function where data can be mapped before being sent as a response.
     * @returns ApiBuilder instance
     */
    setGetMapFunction(middleware) {
        this._getMapFunction = middleware;
        return this;
    }

    /**
     * Disable the delete function (enabled by default)
     * @returns ApiBuilder instance
     */
    disableDelete() {
        this._hasDelete = false;
        return this;
    }

    /**
     * Enable the delete function (enabled by default)
     * @returns ApiBuilder instance
     */
    enableDelete() {
        this._hasDelete = true;
        return this;
    }

    /**
     * Disable the put function (enabled by default)
     * @returns ApiBuilder instance
     */
    disablePut() {
        this._hasPut = false;
        return this;
    }

    /**
     * Enable the put function (enabled by default)
     * @returns ApiBuilder instance
     */
    enablePut() {
        this._hasPut = true;
        return this;
    }

    /**
     * Disable the get function (enabled by default)
     * @returns ApiBuilder instance
     */
    disableGet() {
        this._hasGet = false;
        return this;
    }

    /**
     * Enable the get function (enabled by default)
     * @returns ApiBuilder instance
     */
    enableGet() {
        this._hasGet = true;
        return this;
    }

    /**
     * Disable the post function (enabled by default)
     * @returns ApiBuilder instance
     */
    disablePost() {
        this._hasPost = false;
        return this;
    }

    /**
     * Enable the post function (enabled by default)
     * @returns ApiBuilder instance
     */
    enablePost() {
        this._hasPost = true;
        return this;
    }


    /**
     * Generates the express api function
     * @returns Express function
     */
    build() {
        const router = express.Router();
        if (this._beforefunction) {
            router.use('/' + this._routename, this._beforefunction);
        }
        if (this._hasGet) {
            router.get('/' + this._routename, (req, res) => {
                for (let key in req.query) {
                    try {
                        req.query[key] = JSON.parse(req.query[key]);
                    } catch (e) {
                        //don't worry about it
                    }
                }
                let getPromise = this._routeTable.getAll(req.query);
                if (this._getMapFunction) {
                    getPromise = getPromise.then(d => this._getMapFunction(req, d));
                }
                getPromise.then(json(res)).catch(error(res));
            });
        }

        if (this._hasPost) {
            router.post('/' + this._routename, async (req, res) => {
                try {
                    let data;
                    data = await this._routeTable.create(Object.assign(req.query, req.body));
                    if (this._afterCreateMiddleware) {
                        await this._afterCreateMiddleware(req, data.dataValues);
                    }
                    res.json(data.dataValues);
                }
                catch (e) {
                    handleerror(e, res);
                }
            });
        }

        if (this._hasPut) {
            router.put('/' + this._routename, (req, res) => {
                this._routeTable.update(Object.assign(req.query, req.body)).then(() => res.send('ok')).catch(error(res));
            });
        }

        if (this._hasDelete) {
            router.delete('/' + this._routename, (req, res) => {
                this._routeTable.delete(Object.assign(req.query, req.body)).then(() => res.send('ok')).catch(error(res));
            });
        }

        return router;
    }
};