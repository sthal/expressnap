const Table = require('../lib/table');
const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:', {
    define: {
        freezeTableName: true,
        timestamps: false
    },
    logging: false
});



class TestTable extends Model { }
TestTable.init({
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    age: {
        type: DataTypes.INTEGER,
    },
    name: {
        type: DataTypes.STRING,
    },
    has_email: {
        type: DataTypes.BOOLEAN
    }
}, { sequelize });

class MpkTable extends Model { }
MpkTable.init({
    id1: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    id2: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    sometext: {
        type: DataTypes.STRING
    }
}, { sequelize });

class MultiDataTypeTable extends Model { }
MultiDataTypeTable.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING
    },
    createdate: {
        type: DataTypes.DATE
    },
    flag: {
        type: DataTypes.BOOLEAN
    },
    somenumber: {
        type: DataTypes.INTEGER
    }
}, { sequelize });


describe('table', () => {
    beforeAll(async () => {
        await TestTable.sync({ force: true });
        await MpkTable.sync({ force: true });
        await MultiDataTypeTable.sync({ force: true });
    }, 5000);

    test('insert and find', async () => {
        const testtable = new Table(TestTable);
        await testtable.create({ id: 3, age: 13, name: 'test', has_email: true });

        const result = await testtable.getById(3);
        expect(result).toEqual({ id: 3, age: 13, name: 'test', has_email: true });
    });

    test('update', async () => {
        const testtable = new Table(TestTable);
        await testtable.create({ id: 2, age: 13 });
        await testtable.update({ id: 2, age: 19 });
        const result = await testtable.getById(2);
        expect(result).toEqual({ id: 2, age: 19, name: null, has_email: null });
    });
    test('update multi pk', async () => {
        const testtable = new Table(MpkTable);
        await testtable.create({ id1: 10, id2: 20, sometext: '13' });
        await testtable.update({ id1: 10, id2: 20, sometext: '19' });
        const result = await testtable.getById({ id1: 10, id2: 20 });
        expect(result).toEqual({ id1: 10, id2: 20, sometext: '19' });
    });

    test('delete', async () => {
        const testtable = new Table(TestTable);
        await testtable.create({ id: 5, age: 13 });
        expect(await testtable.getById(5)).toEqual({ id: 5, age: 13, name: null, has_email: null });
        await testtable.delete({ id: 5, age: -1 });
        const result = await testtable.getById(5);
        expect(result).toBeNull();
    });

    test('delete multi pk', async () => {
        const testtable = new Table(MpkTable);
        await testtable.create({ id1: 20, id2: 20, sometext: 'hello wosld' });
        expect(await testtable.getById({ id1: 20, id2: 20 })).toEqual({ id1: 20, id2: 20, sometext: 'hello wosld' });
        await testtable.delete({ id1: 20, id2: 20, sometext: 'data' });
        const result = await testtable.getById({ id1: 20, id2: 20 });
        expect(result).toBeNull();
    });

    test('delete_empty_id', async () => {
        const testtable = new Table(TestTable);
        try {
            await testtable.delete({});
            expect(true).toBeFalsy();
        } catch (e) {
            expect(e.message).toEqual('TestTable: id[id] not present');
        }
    });

    test('update_empty_id', async () => {
        const testtable = new Table(TestTable);
        try {
            await testtable.update({});
            expect(true).toBeFalsy();
        } catch (e) {
            expect(e.message).toEqual('TestTable: id[id] not present');
        }
    });


    test('get_multiple_PKs', async () => {
        expect.assertions(9);
        const testTable = await new Table(MpkTable);
        const testData = [
            { id1: 1, id2: 1, sometext: 'hello world!' },
            { id1: 1, id2: 2, sometext: 'not hello world!' },
            { id1: 1, id2: 3, sometext: 'Lorem Ipsum' },
            { id1: 1, id2: 4, sometext: 'muspi merol' }
        ];
        for (let x of testData) {
            await testTable.create(x);
        }
        await expect(testTable.getById({})).rejects.toEqual(new Error('TableError: id1,id2 is/are missing for a search by the primary key of the table MpkTable!'));
        await expect(testTable.getById({ id: 1 })).rejects.toEqual(new Error('TableError: id is not part of the primary key of the table MpkTable!'));
        await expect(testTable.getById({ id1: 1 })).rejects.toEqual(new Error('TableError: id2 is/are missing for a search by the primary key of the table MpkTable!'));
        await expect(testTable.getById({ id: 3, id2: 2, id3: 1 })).rejects.toEqual(new Error('TableError: id,id3 is not part of the primary key of the table MpkTable!'));


        expect(await testTable.getById({ id1: 3, id2: 2 })).toBeNull();
        expect(await testTable.getById({ id1: 1, id2: 0 })).toBeNull();

        expect(await testTable.getById({ id1: testData[1].id1, id2: testData[1].id2 })).toEqual(testData[1]);
        expect(await testTable.getById({ id1: testData[2].id1, id2: testData[2].id2 })).toEqual(testData[2]);
        expect(await testTable.getById({ id1: testData[3].id1, id2: testData[3].id2 })).toEqual(testData[3]);
    });

    test('error when no id and no auto increment', async () => {
        expect.assertions(1);
        const testTable = await new Table(TestTable);
        await expect(testTable.create({ age: 18, name: 'testperson' })).rejects.toEqual(new Error('TestTable: id[id] not present'));
    });

    test('error when no id and no auto increment multiple primary keys on create', async () => {
        expect.assertions(1);
        const testTable = await new Table(MpkTable);
        await expect(testTable.create({ sometext: 'testperson' })).rejects.toEqual(new Error('MpkTable: id[id1] not present'));
    });
    test('error when no id and no auto increment multiple primary keys on update', async () => {
        expect.assertions(1);
        const testTable = await new Table(MpkTable);
        await expect(testTable.update({ sometext: 'testperson' })).rejects.toEqual(new Error('MpkTable: id[id1] not present'));
    });
    test('error when no id and no auto increment multiple primary keys on delete', async () => {
        expect.assertions(1);
        const testTable = await new Table(MpkTable);
        await expect(testTable.delete({ sometext: 'testperson' })).rejects.toEqual(new Error('MpkTable: id[id1] not present'));
    });

    test('get by id multiple pk single id', async () => {
        expect.assertions(1);
        const testTable = await new Table(MpkTable);
        await expect(testTable.getById('teststring')).rejects.toEqual(new Error('TableError: Id has to be provided as object!'));
    });

    test('parameter parsing', async () => {
        expect.assertions(3);
        const testTable = await new Table(MultiDataTypeTable);
        testTable.create({
            id: 1, name: 'testobject',
            createdate: new Date(2022, 1, 1, 0, 0),
            flag: 'null',
            somenumber: '50'
        });
        expect(await testTable.getById(1)).toEqual({ id: 1, name: 'testobject', createdate: new Date(2022, 1, 1, 0, 0), flag: 'null', somenumber: 50 });
        testTable.create({
            id: 2, name: 'testobject',
            createdate: new Date(2022, 1, 1, 0, 0),
            flag: 'true',
            somenumber: '50'
        });
        expect(await testTable.getById(2)).toEqual({ id: 2, name: 'testobject', createdate: new Date(2022, 1, 1, 0, 0), flag: true, somenumber: 50 });
        testTable.create({
            id: 3, name: 'testobject',
            createdate: new Date(2022, 1, 1, 0, 0),
            flag: 'false',
            somenumber: '50'
        });
        expect(await testTable.getById(3)).toEqual({ id: 3, name: 'testobject', createdate: new Date(2022, 1, 1, 0, 0), flag: false, somenumber: 50 });
    });

});