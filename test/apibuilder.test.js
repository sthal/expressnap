const request = require('supertest');
const express = require('express');
var bodyParser = require('body-parser');

const ApiBuilder = require('../lib/rest/api-builder');
const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:', {
    define: {
        freezeTableName: true,
        timestamps: false
    },
    logging: false
});

class TestTable extends Model { }
TestTable.init({
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    age: {
        type: DataTypes.INTEGER,
    },
    name: {
        type: DataTypes.STRING,
    },
    has_email: {
        type: DataTypes.BOOLEAN
    }
}, { sequelize });

class MpkTable extends Model { }
MpkTable.init({
    id1: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    id2: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    sometext: {
        type: DataTypes.STRING
    }
}, { sequelize });

describe('table', () => {
    beforeAll(async () => {
        await TestTable.sync({ force: true });
        await MpkTable.sync({ force: true });
    }, 5000);

    let app;

    beforeEach(() => {
        app = express();
        app.use(bodyParser.json());
        app.use(new ApiBuilder('test', TestTable).build());
    });


    test('insert and find', async () => {
        const res = await request(app)
            .post('/test')
            .send({ id: 1, age: 18, name: 'testperson', has_email: true });
        expect(res.statusCode).toEqual(200);

        const retrieveRes = await request(app)
            .get('/test');
        expect(retrieveRes.body.find(e => e.age == 18 && e.name == 'testperson' && e.has_email === true)).toBeTruthy();
    });

    test('find non existent entry returns empty list', async () => {
        const res = await request(app).get('/test?id=1000');
        expect(res.statusCode).toEqual(200);
        expect(res.body.length).toEqual(0);
    });

    test('find multiple entries', async () => {
        //add entries
        await request(app)
            .post('/test')
            .send({ id: 10, age: 20, name: 'testpos1' });
        await request(app)
            .post('/test')
            .send({ id: 11, age: 20, name: 'testpos2' });
        await request(app)
            .post('/test')
            .send({ id: 12, age: 20, name: 'testpos3' });
        await request(app)
            .post('/test')
            .send({ id: 13, age: 21, name: 'testneg13' });

        //validate filtering
        const res = await request(app).get('/test?age=20');
        expect(res.statusCode).toEqual(200);
        expect(res.body.length).toEqual(3);
        expect(res.body.map(b => b.age)).toEqual(expect.arrayContaining([20, 20, 20]));
        expect(res.body.map(b => b.name)).toEqual(expect.arrayContaining(['testpos1', 'testpos2', 'testpos3']));
    });

    test('update element', async () => {
        await request(app)
            .post('/test')
            .send({ id: 20, age: 18, name: 'max' });
        const updateRequest = await request(app)
            .put('/test')
            .send({ id: 20, age: 30, name: 'moritz' });
        expect(updateRequest.statusCode).toEqual(200);
        const res = await request(app)
            .get('/test?id=20');
        expect(res.body[0].age).toEqual(30);
        expect(res.body[0].name).toEqual('moritz');
    });

    test('delete element', async () => {
        await request(app)
            .post('/test')
            .send({ id: 30, age: 18, name: 'testdelete' });
        const delRequest = await request(app)
            .delete('/test')
            .send({ id: 30 });
        expect(delRequest.statusCode).toEqual(200);
        const res = await request(app)
            .get('/test?id=30');
        expect(res.statusCode).toEqual(200);
        expect(res.body.length).toEqual(0);
    });

    test('delete non existing element fails', async () => {
        const res = await request(app)
            .delete('/test')
            .send({ id: 31 });
        expect(res.statusCode).toEqual(400);
        expect(res.text).toEqual('0:1:row does not exist in database');
    });

    test('update non existent element', async () => {
        const res = await request(app)
            .put('/test')
            .send({ id: 31, name: 'test' });
        expect(res.statusCode).toEqual(400);
        expect(res.text).toEqual('0:1:row does not exist in database');
    });

    test('post duplicate id returns error', async () => {
        const res = await request(app)
            .post('/test')
            .send({ id: 32, name: 'testduplicate' });
        expect(res.statusCode).toEqual(200);
        const resdup = await request(app)
            .post('/test')
            .send({ id: 32, name: 'testduplicate' });
        expect(resdup.statusCode).toEqual(400);
        expect(resdup.text).toEqual('0:2: unique constrait error for fields id');
    });

    test('disabled get is not available', async () => {
        let instance = express();
        instance.use(bodyParser.json());
        instance.use(new ApiBuilder('testnoget', TestTable).disableGet().build());
        const res = await request(instance).get('/testnoget');
        expect(res.statusCode).toEqual(404);

        const respost = await request(instance)
            .post('/testnoget')
            .send({ id: 41, name: 'testpost', age: 18 });
        expect(respost.statusCode).toEqual(200);
    });

    test('disabled post is not available', async () => {
        let instance = express();
        instance.use(bodyParser.json());
        instance.use(new ApiBuilder('testnopost', TestTable).disablePost().build());
        const res = await request(instance).post('/testnopost');
        expect(res.statusCode).toEqual(404);

        const resget = await request(instance).get('/testnopost');
        expect(resget.statusCode).toEqual(200);
    });

    test('disabled put is not available', async () => {
        let instance = express();
        instance.use(bodyParser.json());
        instance.use(new ApiBuilder('testnoput', TestTable).disablePut().build());
        const res = await request(instance).put('/testnoput');
        expect(res.statusCode).toEqual(404);
        const respost = await request(instance)
            .post('/testnoput')
            .send({ id: 42, name: 'testpost', age: 18 });
        expect(respost.statusCode).toEqual(200);
    });

    test('disabled delete is not available', async () => {
        let instance = express();
        instance.use(bodyParser.json());
        instance.use(new ApiBuilder('testnodelete', TestTable).disableDelete().build());
        const res = await request(instance).delete('/testnodelete');
        expect(res.statusCode).toEqual(404);

        const resget = await request(instance).get('/testnodelete');
        expect(resget.statusCode).toEqual(200);
    });

    test('beforefunction is called and can return arbitrary responses.', async () => {
        const mockCallback = jest.fn((req, res, next) => res.status(418).send('hi from mock'));
        let instance = express();
        instance.use(bodyParser.json());
        instance.use(new ApiBuilder('testbefore', TestTable).setBeforeMiddleware(mockCallback).build());
        const res = await request(instance).get('/testbefore');
        expect(res.statusCode).toEqual(418);
        expect(res.text).toEqual('hi from mock');
        expect(mockCallback.mock.calls.length).toBe(1);
    });

    test('beforefunction is called and normal results will be returned.', async () => {
        const mockCallback = jest.fn((req, res, next) => next());
        let instance = express();
        instance.use(bodyParser.json());
        instance.use(new ApiBuilder('testbeforenormal', TestTable).setBeforeMiddleware(mockCallback).build());
        const res = await request(instance)
            .post('/testbeforenormal')
            .send({ id: 51, age: 18, name: 'testperson', has_email: true });
        expect(res.statusCode).toEqual(200);

        const retrieveRes = await request(instance)
            .get('/testbeforenormal?id=51');
        expect(retrieveRes.body.find(e => e.age == 18 && e.name == 'testperson' && e.has_email === true)).toBeTruthy();

        expect(mockCallback.mock.calls.length).toBe(2);
    });

    test('getmapfunction is called and can alter returned values', async () => {
        const mockCallback = jest.fn((req, data) => ({ hello: 'world' }));
        let instance = express();
        instance.use(bodyParser.json());
        instance.use(new ApiBuilder('testgetmapfunction', TestTable).setGetMapFunction(mockCallback).build());
        for (let i = 0; i < 2; i++) {
            const res = await request(instance)
                .post('/testgetmapfunction')
                .send({ id: 60 + i, age: 69, name: 'getmap', has_email: true });
            expect(res.statusCode).toEqual(200);
        }

        const retrieveRes = await request(instance)
            .get('/testgetmapfunction?name=getmap');
        expect(retrieveRes.body).toEqual({ hello: 'world' });
        expect(mockCallback.mock.calls.length).toBe(1);
        expect(mockCallback.mock.calls[0][1]).toEqual(expect.arrayContaining([{ id: 60, age: 69, name: 'getmap', has_email: true }, { id: 61, age: 69, name: 'getmap', has_email: true }]));
    });

    test('use setAfterCreateMiddleware', async () => {
        const mockCallback = jest.fn((req, data) => data);
        let instance = express();
        instance.use(bodyParser.json());
        instance.use(new ApiBuilder('testaftercreate', TestTable).setAfterCreateMiddleware(mockCallback).build());
        const res = await request(instance)
            .post('/testaftercreate')
            .send({ id: 52, age: 18, name: 'testperson', has_email: true });
        expect(res.statusCode).toEqual(200);
        expect(res.body).toEqual({ id: 52, age: 18, name: 'testperson', has_email: true });
        expect(mockCallback.mock.calls.length).toEqual(1);
        expect(mockCallback.mock.calls[0][1]).toEqual({ id: 52, age: 18, name: 'testperson', has_email: true });
    });
});