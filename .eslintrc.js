module.exports = {
    'env': {
        'browser': false,
        'es6': true,
        'node': true
    },
    'extends': [
        'eslint:recommended'
    ],
    'parserOptions': {
        'sourceType': 'module',
        'ecmaVersion': 2017
    },
    'rules': {
        'indent': [
            'error',
            4,
            {
                'SwitchCase': 1
            }
        ],
        'linebreak-style': [
            'error',
            'unix'
        ],
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'always'
        ],
        'no-console': 0,
        'no-unused-vars': 'warn',
    },
    'globals': {
        expect: true,
        test: true,
        jest: true,
        describe: true,
        beforeAll: true,
        beforeEach: true

    },
    'plugins': [
    ]
};